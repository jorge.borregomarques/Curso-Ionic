import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Componente } from 'src/app/interfaces/interfaces';
import { delay } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpCtrl: HttpClient) { }

  getUsuarios(){
    return this.httpCtrl.get('https://jsonplaceholder.typicode.com/users');
  }

  getAlbums(){
    return this.httpCtrl.get<Componente[]>('https://jsonplaceholder.typicode.com/albums');
  }

  getMenuOpt(){
    return this.httpCtrl.get<Componente[]>('/assets/data/menu-opts.json');
  }
  getSuperHero(){
    return this.httpCtrl.get<Componente[]>('/assets/data/superheroes.json')
              .pipe(
                delay(1500)
              );
  }

}
