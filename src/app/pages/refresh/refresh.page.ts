import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refresh',
  templateUrl: './refresh.page.html',
  styleUrls: ['./refresh.page.scss'],
})
export class RefreshPage implements OnInit {

  items = [];

  constructor() { }

  ngOnInit() {
  }

  doRefresh(event){
    setTimeout(() => {
      this.items = Array(5);
      // Any calls to load data go here
      event.target.complete();
    }, 1500);
  }

}
