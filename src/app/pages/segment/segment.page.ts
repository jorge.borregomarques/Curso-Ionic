import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  superHeroes: Observable<any>;
  publisher = '';

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.superHeroes = this.dataService.getSuperHero();
  }

  segmentChanged(event) {

    // if( event.detail.value === 'todos') {

    //   return this.publisher = '';

    // }

    console.log(event.detail.value);
    this.publisher = event.detail.value;

  }

}
